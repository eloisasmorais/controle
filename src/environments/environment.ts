// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyB_bHIZLT8vC-VQoS_LHN50yVCwTyGu-AM',
    authDomain: 'controle-te1if.firebaseapp.com',
    projectId: 'controle-te1if',
    storageBucket: 'controle-te1if.appspot.com',
    messagingSenderId: '1012488356119',
    appId: '1:1012488356119:web:0b0513b933af78d59a3752',
    measurementId: 'G-FDTDJ0MS3T',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
