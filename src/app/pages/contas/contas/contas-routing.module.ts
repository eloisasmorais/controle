import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CadastroPage } from '../cadastro/cadastro.page';
import { PagarPage } from '../pagar/pagar.page';
import { ReceberPage } from '../receber/receber.page';
import { RelatorioPage } from '../relatorio/relatorio.page';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'pagar',
        component: PagarPage,
      },
      {
        path: 'pagar',
        component: ReceberPage,
      },
      {
        path: 'cadastro',
        component: CadastroPage,
      },
      {
        path: 'relatorio',
        component: RelatorioPage,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [PagarPage, ReceberPage, CadastroPage, RelatorioPage],
})
export class ContasRoutingModule {}
