import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from '../service/login.service';

@Component({
  selector: 'forgot',
  templateUrl: './forgot.page.html',
  styleUrls: ['./forgot.page.scss'],
})
export class ForgotPage implements OnInit {
  registerForm: FormGroup;
  constructor(private builder: FormBuilder, private service: LoginService) {}

  ngOnInit() {
    const { email, required, minLength } = Validators;

    this.registerForm = this.builder.group({
      email: ['', [email, required]],
    });
  }

  recoverPass() {
    const { email } = this.registerForm.value;
    this.service.recoverPass(email);
  }
}
