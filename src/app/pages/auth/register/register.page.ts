import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from '../service/login.service';

@Component({
  selector: 'register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  registerForm: FormGroup;
  nome;
  sobrenome;
  email;
  password;
  confirm_pass;
  constructor(private builder: FormBuilder, private service: LoginService) {}

  ngOnInit() {
    const { email, required, maxLength, minLength } = Validators;

    this.registerForm = this.builder.group({
      nome: ['', [required, minLength(2), maxLength(19)]],
      sobrenome: ['', [required, minLength(2), maxLength(19)]],
      email: ['', [email, required]],
      password: ['', [required, minLength(8)]],
      confirm_pass: ['', [required, minLength(8)]],
    });
  }

  createUser() {
    const user = this.registerForm.value;
    this.service.createUser(user);
  }
}
