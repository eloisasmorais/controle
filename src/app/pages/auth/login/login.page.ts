import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { LoginService } from '../service/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;
  email;
  password;
  constructor(
    private builder: FormBuilder,
    private service: LoginService,
    private nav: NavController
  ) {}

  ngOnInit() {
    this.checkLogin();

    const { email, required, minLength } = Validators;

    this.loginForm = this.builder.group({
      email: ['', [email, required]],
      password: ['', [required, minLength(8)]],
    });
  }

  checkLogin() {
    this.service.isLoggedIn.subscribe((user) => {
      if (user) this.nav.navigateForward('/home');
    });
  }

  login() {
    const user = this.loginForm.value;

    this.service.login(user);
  }
}
