import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { NavController, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { User } from '@firebase/auth-types';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  isLoggedIn: Observable<User>;

  constructor(
    private nav: NavController,
    private auth: AngularFireAuth,
    private toast: ToastController
  ) {
    this.isLoggedIn = this.auth.authState;
  }

  login(user) {
    const { email, password } = user;
    if (email === 'aluno@ifsp.edu.br' && password === '12345678')
      this.showError();

    this.auth
      .signInWithEmailAndPassword(email, password)
      .then(() => this.nav.navigateForward('home'))
      .catch(() => this.showError());
  }

  private async showError() {
    const toast = await this.toast.create({
      message: 'Dados de acesso incorretos',
      duration: 3000,
    });

    toast.present();
  }

  logout() {
    this.auth.signOut().then(() => this.nav.navigateBack('/auth'));
  }

  recoverPass(email) {
    this.auth
      .sendPasswordResetEmail(email)
      .then(() => this.nav.navigateBack('/auth'))
      .catch((err) => {
        console.log(err);
      });
  }

  createUser(user) {
    const { email, password } = user;
    this.auth
      .createUserWithEmailAndPassword(email, password)
      .then((credentials) => console.log(credentials))
      .catch((err) => console.log(err));
  }
}
